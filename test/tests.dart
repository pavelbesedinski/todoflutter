import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todolistapp/data/CheckboxData.dart';
import 'package:todolistapp/widgets/CheckboxElement.dart';
import 'package:todolistapp/widgets/ToDoApp.dart';

void main() {
  /// Поиск кнопок
  testWidgets('Find buttons', (WidgetTester tester) async {

    await tester.pumpWidget(ToDoApp());
    expect(find.text('Pick All'), findsOneWidget);
    expect(find.text('Delete All'), findsOneWidget);
    expect(find.byIcon(Icons.add), findsOneWidget);
  });

  /// Поиск CheckboxElement виджетов
  testWidgets('Find CheckboxElements', (WidgetTester tester) async {
    var checkboxDataList = [
      CheckboxData(id: 0, task: '0', picked: false),
      CheckboxData(id: 1, task: '1', picked: false),
      CheckboxData(id: 2, task: '2', picked: false),
      CheckboxData(id: 3, task: '3', picked: false),
    ];

    List<Widget> checkboxElementList = checkboxDataList.map((e) => CheckboxElement(checkboxData: e)).toList();

    var widgetTree = (List<Widget> children) => MaterialApp(
      home: Scaffold(
        body: ListView(
          children: children,
        ),
      )
    );

    await tester.pumpWidget(widgetTree(checkboxElementList));
    for (int i = 0; i < checkboxElementList.length; i++) {
      expect(find.byWidget(checkboxElementList[i]), findsWidgets);
    }
  });
}