import 'package:flutter/material.dart';

import 'package:todolistapp/data/CheckboxData.dart';
import 'package:todolistapp/widgets/CheckboxElement.dart';
import 'package:todolistapp/widgets/ControlButton.dart';
import 'package:todolistapp/data/TaskData.dart';

class ToDoApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ToDoAppState();
  }
}

class ToDoAppState extends State<ToDoApp> {
  /// Поле `_elements` хранит количества использованных блоков
  static int _elements = 0;
  /// Поле `THEME_COLOR` задаёт цвет для элементов виджетов
  static const Color THEME_COLOR = Colors.cyan;

  /// Поле `_taskList` содержит список данных типа `CheckboxData`, описывающих
  /// блоки Checkbox
  List<CheckboxData> _taskList = [];

  ScrollController _scrollController;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: Text('ToDo'),
            backgroundColor: THEME_COLOR,
          ),
          body: ListView.builder(
            controller: _scrollController,
            itemCount: _buildList().length,
            itemBuilder: (context, index) {
              return _buildList()[index];
            },
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: _addElement,
            backgroundColor: THEME_COLOR,
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                ControlButton(text: 'Pick All', color: THEME_COLOR, action: _pickAll),
                ControlButton(text: 'Delete All', color: THEME_COLOR, action: _deleteElemenets)
              ],
            ),
          ),
        )
    );
  }

  /// Метод `_buildList` возвращает список виджетов `CheckboxElement`,
  /// построенных на основе списка `_taskList` типа `CheckboxData`
   List<Widget> _buildList() {
    return _taskList.map((CheckboxData checkboxData) => CheckboxElement(checkboxData: checkboxData)).toList();
  }

  /// Метод `_addElement` создаёт новый Checkbox
  void _addElement() {
    setState(() {
      _taskList.add(CheckboxData(id: _elements, task: _elements.toString(), picked: false));
    });
    _elements++;
    _scrollBottom();
  }

  /// Метод `_scrollBottom` выполняет скроллинг к низу экрана
  void _scrollBottom() {
    _scrollController.animateTo(_scrollController.position.maxScrollExtent + 80,
        duration: Duration(milliseconds: 500), curve: Curves.linear);
  }

  /// Метод `_scrollTop` выполняет скроллинг к верху экрана
  void _scrollTop() {
    _scrollController.animateTo(_scrollController.position.minScrollExtent,
        duration: Duration(milliseconds: 100), curve: Curves.fastLinearToSlowEaseIn);
  }

  /// Метод `_isAllElementsPicked` проверяет все ли Checkbox'ы выбраны
  /// пользователем, метод возвращает true, если все поля `picked` списка
  /// `_taskList` равны true, иначе - false
  bool _isAllElementsPicked() {
    for (int i = 0; i < _taskList.length; i++) {
      if(!_taskList[i].picked) return false;
    }
    return true;
  }

  /// Метод `_pickALl`, если не выбран хотя бы один Checkbox, меняет значение
  /// поля `picked` всех Checkbox'ов на true, в ином случае - на false
  void _pickAll() {
    bool pickedAll = _isAllElementsPicked();
    for (CheckboxData checkboxData in _taskList) {
      setState(() {
        checkboxData.picked = pickedAll ? false : true;
      });
    }
  }

  /// Метод `_deleteAll` очищает список Checkbox'ов
  void _deleteElemenets() {
    if(_isAllElementsPicked() || _taskList.where((element) => element.picked).length == 0) setState(() {_taskList = [];});
    else _deletePickedElements();
    _scrollTop();
  }

  /// Метод `_deletePickedElements` выполняет очищение выбранных Checkbox'ов
  void _deletePickedElements() {
    _taskList.removeWhere((element) => element.picked == true);
    setState(() {});
  }

  /// Метод `_loadData` инициализирует список Checkbox'ов начальными элементами
  void _loadData() {
    var dataList = List<CheckboxData>();
    var allData = TaskData.data;
    allData.forEach((key, value) {
      dataList.add(
        CheckboxData(
          id: key,
          task: value,
          picked: false,
        ),
      );
    });

    setState(() {
      _elements = dataList.length;
      _taskList = dataList;
    });
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _loadData();
    super.initState();
  }

}