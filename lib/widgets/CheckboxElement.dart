import 'package:flutter/material.dart';

import 'package:flutter/widgets.dart';
import 'package:todolistapp/data/CheckboxData.dart';

///
/// Класс `CheckboxElement` описывает виджет используемый для построения Checkbox блока
///
class CheckboxElement extends StatefulWidget {
  /// Поле `checkboxData` содержит информацию о Checkbox блоке
  final CheckboxData checkboxData;

  CheckboxElement({@required this.checkboxData}): assert(checkboxData != null);

  @override
  State<StatefulWidget> createState() {
    return CheckboxElementState();
  }
}

class CheckboxElementState extends State<CheckboxElement> {
  /// Константы задают цвет элементов виджетов
  static const Color ACTIVATED_CHECKBOX_COLOR = Colors.green;
  static const  Color DEACTIVATED_CHECKBOX_COLOR = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 2.0),
      child: Card(
        color: DEACTIVATED_CHECKBOX_COLOR,
        shape: Border(
            right: BorderSide(
                color: widget.checkboxData.picked ? ACTIVATED_CHECKBOX_COLOR:DEACTIVATED_CHECKBOX_COLOR,
                width: 5
            ),
        ),
        child: ListTile(
          onTap: () {
            _changeCheckedValue(!widget.checkboxData.picked);
            },
          leading: Checkbox(
            activeColor: ACTIVATED_CHECKBOX_COLOR,
            value: widget.checkboxData.picked,
            onChanged: (value) {
              _changeCheckedValue(value);
              },
          ),
          title: Text('id: ${widget.checkboxData.id}'),
          subtitle: Text('Task ${widget.checkboxData.task}'),
        ),
      ),
    );
  }

  /// Метод `_changeCheckedValue` меняет состояние Checkbox'а
  void _changeCheckedValue(bool value) {
    setState(() {
      widget.checkboxData.picked = value;
    });
  }
}