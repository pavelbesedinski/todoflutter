import 'package:flutter/material.dart';

///
/// Класс `ControlButton` описывает виджет используемый в качестве кнопки
///
class ControlButton extends StatefulWidget {
  /// String text     - определяет текст кнопки
  /// Color color     - определяет цвет кнопки
  /// Function action - определяет функцию, которая выполняется
  ///                   по нажатию на кнопку
  final String text;
  final Color color;
  final Function action;

  ControlButton({this.text = "", this.color = Colors.lightBlue, this.action});

  @override
  State<StatefulWidget> createState() {
    return ControlButtonState();
  }
}

class ControlButtonState extends State<ControlButton> {
  @override
  Widget build(BuildContext context) {
    return new MaterialButton(
      height: 45.0,
      minWidth: 110.0,
      color: widget.color,
      textColor: Colors.white,
      child: new Text("${widget.text}"),
      onPressed: widget.action,
    );
  }
}