///
/// Класс `TaskData` содержит начальный список ToDo
///
class TaskData {
  static const int MAX_TASK_COUNT = 5;
  static var data = {for (int i = 0; i < MAX_TASK_COUNT; i++) i:i.toString()};
}