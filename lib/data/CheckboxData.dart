///
/// Класс `CheckboxData` описывает модель данных в блоке checkbox
///
class CheckboxData {
  final int id;
  final String task;
  bool picked;
  CheckboxData({this.id, this.task, this.picked});
}